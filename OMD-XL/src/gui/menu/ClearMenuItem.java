package gui.menu;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JMenuItem;
import model.Model;
import gui.CurrentSlot;

class ClearMenuItem extends JMenuItem implements ActionListener {
	private Model sheet;
	private CurrentSlot currentSlot;
	
    public ClearMenuItem(CurrentSlot currentSlot, Model sheet) {
        super("Clear");
        this.sheet = sheet;
        this.currentSlot = currentSlot;
        addActionListener(this);
    }
    public void actionPerformed(ActionEvent e) {
    	sheet.removeSlot(currentSlot.getName());
    }
}