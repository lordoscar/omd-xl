package gui.menu;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.HashMap;
import java.util.Iterator;

import model.Factory;
import model.Model;
import model.Slot;
import util.XLException;

//TODO move to another package
public class XLBufferedReader extends BufferedReader {
	
	private Factory factory = new Factory();
	
    public XLBufferedReader(String name) throws FileNotFoundException {
        super(new FileReader(name));
    }

    public void load(HashMap<String, Slot> temp) {
        try {        	
            while (ready()) {
                String string =  readLine();
            	System.out.println("läst line: " + string);
                int i = string.indexOf('=');
                String name = string.substring(0, i);
                String expr = string.substring(i+1);
                temp.put(name, factory.build(expr));
            }
        } catch (Exception e) {
            throw new XLException(e.getMessage());
        }
    }
}
