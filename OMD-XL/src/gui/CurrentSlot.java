package gui;

import java.awt.Color;
import java.util.Observable;

public class CurrentSlot extends Observable {
	private SlotLabel currentSlot;

	public CurrentSlot() {
	}

	public String getName() {
		return currentSlot.getName();
	}

	public void set(SlotLabel slotLabel) {
		currentSlot = slotLabel;
		setChanged();
		notifyObservers();
	}

	public void setWhite() {
		currentSlot.setBackground(Color.WHITE);
	}

	public void clearSlot() {
		currentSlot.setText("");
	}
}
