package model;

import expr.Environment;
import util.XLException;

public class CircualErrorSlot implements Slot {

	@Override
	public double getValue(Environment e) {
		throw new XLException("Circual Error");
	}

	@Override
	public String toString(Environment e) {
		throw new XLException("Circual Error");
	}

	@Override
	public String commentValue() {
		return null;
	}
	
	@Override
	public String savedToString() {
		throw new XLException("Circual Error");
	}


}
