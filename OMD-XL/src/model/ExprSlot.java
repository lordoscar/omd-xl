package model;

import expr.Environment;
import expr.Expr;

public class ExprSlot implements Slot {
	private Expr expr;
	
	public ExprSlot(Expr expr){
		this.expr = expr;
	}
	
	@Override
	public double getValue(Environment e) {
		return expr.value(e);
	}

	@Override
	public String toString(Environment e) {
		return expr.toString();
	}

	@Override
	public String commentValue() {
		return null;
	}

	@Override
	public String savedToString() {
		return expr.toString();
	}
}
