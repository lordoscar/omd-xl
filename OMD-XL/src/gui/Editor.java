package gui;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Observable;
import java.util.Observer;

import javax.swing.JTextField;

import model.Model;
import util.XLException;

public class Editor extends JTextField implements ActionListener, Observer {
	Model sheet;
	StatusLabel statusL;
	CurrentSlot currentSlot;

	public Editor(CurrentSlot currentSlot, StatusLabel statusL, Model sheet) {
		setBackground(Color.WHITE);
		this.sheet = sheet;
		this.statusL = statusL;
		this.currentSlot = currentSlot;
		addActionListener(this);
		currentSlot.addObserver(this);
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		statusL.clear();
		String name = currentSlot.getName();
		if (getText().equals("")) {
			try {
				sheet.removeSlot(name);
			} catch (XLException e) {
				statusL.setText(e.getMessage());
			}
		} else {
			try {
				sheet.putSlot(name, getText());
			} catch (XLException e) {
				statusL.setText(e.getMessage());
			}
		}
	}

	@Override
	public void update(Observable o, Object arg) {
		String name = currentSlot.getName();
		String stringSlot = sheet.getStringValue(name);
		setText(stringSlot);

	}
}