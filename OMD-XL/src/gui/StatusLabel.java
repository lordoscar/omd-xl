package gui;

import java.awt.Color;
import java.util.Observable;
import java.util.Observer;

import model.Model;

public class StatusLabel extends ColoredLabel implements Observer {
	Model m; 
	
    public StatusLabel() {
        super("", Color.WHITE);
        m = new Model();
        m.addObserver(this);
    }

    public void update(Observable o, Object obj) {
    	setText("");
    }
    
    public void clear() {
		 setText("");
	}
}