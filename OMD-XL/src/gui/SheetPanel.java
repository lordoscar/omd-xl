package gui;

import static java.awt.BorderLayout.CENTER;
import static java.awt.BorderLayout.WEST;

import model.Model;

public class SheetPanel extends BorderPanel {
	private SlotLabels slotLabels;
	
    public SheetPanel(int rows, int columns, CurrentSlot currentSlot, Model sheet, StatusLabel statusLabel) {
        add(WEST, new RowLabels(rows));
        slotLabels = new SlotLabels(rows, columns, currentSlot, sheet, statusLabel);
        add(CENTER, slotLabels);
        sheet.addSlotLabels(slotLabels);
    }
}