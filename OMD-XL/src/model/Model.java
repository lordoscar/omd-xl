package model;

import java.util.HashMap;
import java.util.Map.Entry;
import java.util.Observable;
import java.util.Set;
import expr.Environment;
import gui.SlotLabel;
import gui.SlotLabels;
import util.XLException;

public class Model extends Observable implements Environment {
	private HashMap<String, Slot> sheet;
	private Factory factory;
	private SlotLabels slotLabels;

	// Model
	public Model() {
		super();
		sheet = new HashMap<String, Slot>();
		factory = new Factory();
	}

	public Model(HashMap<String, Slot> sheet) {
		this.sheet = sheet;
	}

	@Override
	public double value(String name) {
		Slot slot = sheet.get(name);
		if (slot == null) {
			throw new XLException("Empty Slot: " + name);
		}
		return slot.getValue(this);
	}

	public Slot getSlot(String key) {
		return sheet.get(key);
	}

	public String getSlotValue(String key) {
		Slot s = getSlot(key);
		if (s == null) {
			return "";
		} else if (s instanceof CommentSlot) {
			return s.commentValue();
		}

		return String.valueOf(getSlot(key).getValue(this));
	}
	
	public String getStringValue(String key) {
		Slot s = getSlot(key);
		if (s == null) {
			return "";
		} 
		return s.toString(this);
		
	}

	public void put(String string, Slot slot) {
		sheet.put(string, slot);
		updateSheet();
	}

	public Set<Entry<String, Slot>> entrySet() {
		return sheet.entrySet();
	}

	public int size() {
		return sheet.size();
	}

	public void removeSlot(String key) {
		Slot slot = sheet.get(key);
		Slot errorSlot = new CircualErrorSlot();
		sheet.put(key, errorSlot);
		try {
			for (Slot s : sheet.values()) {
				if (s != errorSlot) {
					s.getValue(this);
				}
			}
		} catch (XLException e) {
			sheet.put(key, slot);
			throw new XLException("Cannot remove cell " + key);
		}
		sheet.remove(key);
		updateSheet();
	}

	public void clearAll() {
		sheet = new HashMap<String, Slot>();
		slotLabels.clearAll();
		updateSheet();
	}

	private void updateSheet() {
		setChanged();
		notifyObservers();
	}

	public void putSlot(String name, String text) {
		Slot slot = factory.build(text);
		checkLoop(name, slot);
		sheet.put(name, slot);
		updateSheet();
	}
	
	public void load(HashMap<String, Slot> newSheet) {
		HashMap<String, Slot> oldSheet = sheet;
		sheet = newSheet;
		try {
			for (Entry<String, Slot> entry : sheet.entrySet()) {
				checkLoop(entry.getKey(), entry.getValue());
			}
		} catch (XLException e) {
			sheet = oldSheet;
			throw e;
		}
		updateSheet();
	}

	private void checkLoop(String name, Slot slot) {
		Slot oldSlot = sheet.get(name);
		Slot errorSlot = new CircualErrorSlot();
		sheet.put(name, errorSlot);
		try {
			slot.getValue(this);
		} finally {
			sheet.put(name, oldSlot);
		}
	}
	
	public void addSlotLabels(SlotLabels slotLabels){
		this.slotLabels = slotLabels; 
	}
	
	public void updateSlotLabels() {
		for (SlotLabel s : slotLabels.getList()) {
			s.updateText();
		}
	}
}