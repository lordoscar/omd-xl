package gui.menu;

import model.Model;
import gui.StatusLabel;
import gui.XL;
import java.io.FileNotFoundException;
import java.util.Set;

import javax.swing.JFileChooser;

class SaveMenuItem extends OpenMenuItem {
	private Model sheet;
	
    public SaveMenuItem(XL xl, StatusLabel statusLabel, Model sheet) {
        super(xl, statusLabel, "Save");
        this.sheet = sheet;
    }
    
    protected void action(String path) throws FileNotFoundException {
    	XLPrintStream writer = new XLPrintStream(path);
    	writer.save(sheet.entrySet());
    }

    protected int openDialog(JFileChooser fileChooser) {
        return fileChooser.showSaveDialog(xl);
    }
}