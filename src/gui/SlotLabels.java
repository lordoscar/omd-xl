package gui;

import java.awt.Color;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import java.util.List;
import javax.swing.SwingConstants;
public class SlotLabels extends GridPanel {
    private List<SlotLabel> labelList;
    private int selectedIndex = -1;
    public SlotLabels(int rows, int cols) {
        super(rows + 1, cols);
        labelList = new ArrayList<SlotLabel>(rows * cols);
        for (char ch = 'A'; ch < 'A' + cols; ch++) {
            add(new ColoredLabel(Character.toString(ch), Color.LIGHT_GRAY,
                    SwingConstants.CENTER));
        }
        for (int row = 1; row <= rows; row++) {
            for (char ch = 'A'; ch < 'A' + cols; ch++) {
                SlotLabel label = new SlotLabel();
                add(label);
                labelList.add(label);
            }
        }
        addMouseListener(ml);
    }
    
    MouseListener ml = new MouseListener()
    {
        public void mouseClicked(java.awt.event.MouseEvent evt) 
        {
        	System.out.println("Upptckt musklick: " + evt.getX() + " " + evt.getY());
            SlotLabels labels = (SlotLabels) evt.getComponent();
            Class compareClass = labels.getComponentAt(evt.getPoint()).getClass();
            if(compareClass == ColoredLabel.class || compareClass == SlotLabels.class){
            	return;
            }
            SlotLabel label = (SlotLabel) labels.getComponentAt(evt.getPoint());
            int x = (label.getX() / 82);
            int y = (label.getY() / 18);
            int index = 8 * (y-1) + x;
            setSelectedLabel(index);
            char X = (char) (x + 'A');
            String print = X + String.valueOf(y);
            System.out.println(print);
        }
		@Override
		public void mousePressed(MouseEvent e) {
			// TODO Auto-generated method stub
			
		}
		@Override
		public void mouseReleased(MouseEvent e) {
			// TODO Auto-generated method stub
			
		}
		@Override
		public void mouseEntered(MouseEvent e) {
			// TODO Auto-generated method stub
			
		}
		@Override
		public void mouseExited(MouseEvent e) {
			// TODO Auto-generated method stub
			
		}
    };
    
    public boolean hasSelected(){
    	return (selectedIndex != -1);
    }
    
    public void setSelectedLabel(int index){
    	if(hasSelected()){
    		removeSelectedLabel();
    	}
    	selectedIndex = index;
    	SlotLabel selectedLabel = labelList.get(index);
    	selectedLabel.setBackground(Color.BLUE);
    }
    
    public void removeSelectedLabel(){
    	SlotLabel selectedLabel = labelList.get(selectedIndex);
    	selectedLabel.setBackground(Color.WHITE);
    }

	public void clearAll() {
		for(SlotLabel label : labelList){
			label.setText(null);
		}
		
	}
}