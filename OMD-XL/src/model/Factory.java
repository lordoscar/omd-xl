package model;

import java.io.IOException;

import expr.*;
import gui.SlotLabels;

public class Factory {

	public Slot build(String string) {
		ExprParser decider = new ExprParser();
		if (string.length() > 0) {
			if (string.charAt(0) == '#') {
				return new CommentSlot(string);
			}
			else {
				try {
					return new ExprSlot(decider.build(string));
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		return null;

		/*
		 * if (string.contains("#")) { Slot comment = new CommentSlot(string);
		 * return comment.toString(); } try { Expr expr = decider.build(string);
		 * Slot exprSlot = new ExprSlot(expr); return exprSlot.getValue(); }
		 * catch (IOException e) { // TODO Auto-generated catch block
		 * e.printStackTrace(); } return null;
		 */

	}
}
