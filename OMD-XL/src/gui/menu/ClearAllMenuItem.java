package gui.menu;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JMenuItem;
import model.Model;

class ClearAllMenuItem extends JMenuItem implements ActionListener {
	private Model sheet;
	
    public ClearAllMenuItem(Model sheet) {
        super("Clear all");
        this.sheet = sheet;
        addActionListener(this);
    }

    public void actionPerformed(ActionEvent e) {
    	sheet.clearAll();
    }
}