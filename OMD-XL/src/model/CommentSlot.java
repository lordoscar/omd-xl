package model;

import expr.Environment;

public class CommentSlot implements Slot {
	private String string;
	
	public CommentSlot(String string){
		this.string = string;
	}
	
	@Override
	public double getValue(Environment e) {
		return 0;
	}

	@Override
	public String toString(Environment e) {
		return string;
	}

	@Override
	public String commentValue() {
		return string;
	}

	@Override
	public String savedToString() {
		return string;
	}
}