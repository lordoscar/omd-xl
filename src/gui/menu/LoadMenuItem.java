package gui.menu;

import gui.StatusLabel;
import gui.XL;
import model.Model;
import util.XLBufferedReader;
import java.io.FileNotFoundException;
import java.util.HashMap;

import javax.swing.JFileChooser;

class LoadMenuItem extends OpenMenuItem {
	private Model content;
 
    public LoadMenuItem(XL xl, StatusLabel statusLabel) {
        super(xl, statusLabel, "Load");
        this.content = content; 
    }

    protected void action(String path) throws FileNotFoundException {  
    	statusLabel.clear();
    	XLBufferedReader br = new XLBufferedReader(path);
    	HashMap<String, String> temp = new HashMap<String, String>();
    	br.load(temp);
    	content.addAll(temp);
    	//uppdatera här med eller? fortfarande ingen controller
    }

    protected int openDialog(JFileChooser fileChooser) {
        return fileChooser.showOpenDialog(xl);
    }
}