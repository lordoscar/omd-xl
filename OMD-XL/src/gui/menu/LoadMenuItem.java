package gui.menu;

import gui.StatusLabel;
import gui.XL;
import model.Model;
import model.Slot;
import java.io.FileNotFoundException;
import java.util.HashMap;

import javax.swing.JFileChooser;

class LoadMenuItem extends OpenMenuItem {
	private Model sheet;
 
    public LoadMenuItem(XL xl, StatusLabel statusLabel, Model sheet) {
        super(xl, statusLabel, "Load");
        this.sheet = sheet; 
    }

    protected void action(String path) throws FileNotFoundException {  
    	statusLabel.clear();
    	
    	try{
    		XLBufferedReader br = new XLBufferedReader(path);
        	HashMap<String, Slot> temp = new HashMap<String, Slot>();
        	br.load(temp);
        	System.out.println(temp.size());
        	sheet.clearAll();
        	sheet.load(temp);
        	sheet.updateSlotLabels();
    	}catch(Exception ex){
    		statusLabel.setText("Error!: " + ex.getMessage());
    	}
    }

    protected int openDialog(JFileChooser fileChooser) {
        return fileChooser.showOpenDialog(xl);
    }
}