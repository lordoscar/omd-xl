	Svar:

    1. Vilka klasser behövs för ett kalkylblad?

Row,
Column, 
Field <- (Blank, Expression, Comment),

 (New, Save, Open, Close)

	2. En ruta i kalkylarket ska innehålla en text eller ett uttryck, hur modellerar man detta?

Genom att göra ett interface "Field" som representerar en ruta, och därefter har man klasser som implementerar detta.

	3. Hur ska man hantera minneskraven?

Sparas i textfil

	4. Observatörer vs observeras?

Observatörer är de klasser som är beroende av expressions, expressions observeras.

	5. Vilket paket och vilken klass håller koll på currentfield?

Gui, current

	6. Vilken funktionalitet funkar?

Programmet (menyrader osv) funkar, kalkylbladet funkar ej.

	7. Var ska fel hanteras?
	
I terminalfönstret, när man sköter input.

	8. Vilken klass används för att representera en adress i ett uttryck?

SlotLabel

	9. Vilken klass ska implementera gränssnittet? Varför använder man inte klassnamnet?

Expression, använder ej klassnamnet pga interfacet är mer "globalt"

	10. Om ett uttryck i kalkylarket refererar till sig själv, direkt eller indirekt, så kommer det att bli bekymmer vid beräkningen av uttryckets värde. Föreslå något sätt att upptäcka sådana cirkulära beroenden! Det finns en elegant lösning med hjälp av strategimönstret som du får chansen att upptäcka. Om du inte hittar den så kommer handledaren att avslöja den.


    User cases:
    
- Användare ska kunna fylla i kommentar i en ruta genom att skriva # före strängen.
- Användare ska se värdet av ett uttryck i rutorna, men se uttrycket i editorn.
- Kommentar plus siffra ska ge ett tal.
- Rekursion i tom ruta (Ex skriva A2 + 1 i A1 när A2 är tom) ger felmeddelande i statusfältet.
- Skriva in B3 på B3 (cirkulära fel) ger felmeddelande i statusfältet.
- Programmet ska kunna spara rutnätet i en .xl-fil.
- Programmet ska kunna ladda sparade .xl-filer, som förs in i rutnätet.