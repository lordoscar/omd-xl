package gui;

import java.awt.Color;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.Observable;
import java.util.Observer;

import model.Model;

public class SlotLabel extends ColoredLabel implements MouseListener, Observer {
	
	private String name;
	private CurrentSlot currentSlot;
	private Model sheet;
	private StatusLabel statusLabel;
	
	public SlotLabel(String name, CurrentSlot currentSlot, Model sheet,
			StatusLabel statusLabel) {
		super("                    ", Color.WHITE, RIGHT);
		this.currentSlot = currentSlot;
		this.name = name;
		this.sheet = sheet;
		addMouseListener(this);
		sheet.addObserver(this);
	}
	
	public String getName() {
		return name;
	}

	@Override
	public void update(Observable o, Object obj) {
		String s = sheet.getSlotValue(name);
		setText(s);
	}
	
	public void updateText() {
		String s = sheet.getSlotValue(name);
		setText(s);
	}

	@Override
	public void mouseClicked(MouseEvent e) {
		currentSlot.setWhite();
		setBackground(Color.YELLOW);
		currentSlot.set(this);
//		controller.notifyObservers();
		if(statusLabel != null){
				statusLabel.clear();
		}
		System.out.println(currentSlot.getName());
	}

	@Override
	public void mousePressed(MouseEvent e) {		
	}

	@Override
	public void mouseReleased(MouseEvent e) {		
	}

	@Override
	public void mouseEntered(MouseEvent e) {		
	}

	@Override
	public void mouseExited(MouseEvent e) {		
	}
}