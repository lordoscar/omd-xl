package gui.menu;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JMenuItem;

import model.Model;

class ClearAllMenuItem extends JMenuItem implements ActionListener {
	private Model content;
	
    public ClearAllMenuItem() {
        super("Clear all");
        this.content = content;
        addActionListener(this);
    }

    public void actionPerformed(ActionEvent e) {
    	content.clearAll();
    }
}