package model;

public class CurrentSlot {
	
	private Slot slot;
	private String address;
	
	public void setCurrentSlot(String address){
		this.address = address;
		//this.slot = model.getValue(address);
	}
	
	public Slot getSlot(){
		return slot;
	}
	
	public String getAddress(){
		return address;
	}

}
