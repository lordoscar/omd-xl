package gui.menu;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JMenuItem;
import gui.SlotLabels;
import model.Model;
import gui.CurrentLabel;
import gui.SlotLabel;

class ClearMenuItem extends JMenuItem implements ActionListener {
	private Model content;
	private CurrentLabel currentLabel;
	
    public ClearMenuItem() {
        super("Clear");
        this.content = content;
        this.currentLabel = currentLabel;
        addActionListener(this);
    }
    public void actionPerformed(ActionEvent e) {
        content.removeSlot(currentLabel.getName());
    }
}